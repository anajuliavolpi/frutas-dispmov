package br.edu.ifsc.listafrutasdispmov.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.edu.ifsc.listafrutasdispmov.R;
import br.edu.ifsc.listafrutasdispmov.R;
import br.edu.ifsc.listafrutasdispmov.models.Fruta;

public  class InfoNutricionaisAdapter extends ArrayAdapter<Fruta> {
    private static  final String TAG = "InfoNutricionaisAdapter";
    private Context mContext;
    private  int mResource;

    /**
     * Default Construtor
     * @param context
     * @param resource
     * @param objects
     */
    public InfoNutricionaisAdapter ( Context context, int resource, ArrayList<Fruta> objects)  {
        super(context, resource, objects);
        mContext=context;
        mResource=resource;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        convertView= inflater.inflate(mResource, parent, false);

        TextView tvNome = (TextView) convertView.findViewById(R.id.tvNome);
        TextView tvInfoNutri = (TextView) convertView.findViewById(R.id.tvInfoNutri);

        tvNome.setText(getItem(position).getNome());
        tvInfoNutri.setText(getItem(position).getInfo_nutricionais());

        return  convertView;
    }
}
