package br.edu.ifsc.listafrutasdispmov.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import br.edu.ifsc.listafrutasdispmov.R;
import br.edu.ifsc.listafrutasdispmov.controler.Frutas;
import br.edu.ifsc.listafrutasdispmov.models.Fruta;
import br.edu.ifsc.listafrutasdispmov.util.InfoNutricionaisAdapter;

public class Info_Nutricionais_Fruta extends AppCompatActivity {

    ArrayList<Fruta> listaFrutas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info__nutricionais__fruta);

        Intent intentFruta = getIntent();

        int posicao = intentFruta.getIntExtra("Posicao", -1);

        final ListView listViewFruta = findViewById(R.id.listView);

        listaFrutas = new ArrayList<Fruta>();

        Frutas frutaController = new Frutas();

        listaFrutas.add(frutaController.FRUTAS[posicao]);

        InfoNutricionaisAdapter adapter = new InfoNutricionaisAdapter(
                getApplicationContext(),
                R.layout.template_info__nutricionais__fruta,
                listaFrutas);

        listViewFruta.setAdapter(adapter);

    }
}
