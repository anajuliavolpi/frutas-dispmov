package br.edu.ifsc.listafrutasdispmov.activitys;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import br.edu.ifsc.listafrutasdispmov.R;
import br.edu.ifsc.listafrutasdispmov.controler.Frutas;
import br.edu.ifsc.listafrutasdispmov.models.Fruta;


public class ListViewSimples extends AppCompatActivity {

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_simples);

        listView = findViewById(R.id.listView);

        Frutas frutaController = new Frutas();

        ArrayList<String> dados = new ArrayList<String>();

        for (Fruta f: frutaController.FRUTAS){
            dados.add(f.getNome());
        }

        ArrayAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.simple_list_item_1,
                dados
        );

        listView.setAdapter(adapter);

        }

   }

