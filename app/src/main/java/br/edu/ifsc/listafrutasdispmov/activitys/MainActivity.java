package br.edu.ifsc.listafrutasdispmov.activitys;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import br.edu.ifsc.listafrutasdispmov.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void abreActivityComListViewPersonalizadaLegal(View v){

        startActivity(new Intent(this,ListViewPersonalizada.class));

    }
    public void abreActivityComListViewSimples(View v){

        startActivity(new Intent(this,ListViewSimples.class));

    }

}
